/*
  The MIT License

  Copyright © 2018 James C. De Ricco

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/

'use strict';

console.info('cube-paint.js: loading');
var scene = new THREE.Scene();

var camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
camera.position.x = 0;
camera.position.y = 0;
camera.position.z = 5;

var renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);

const clock = new THREE.Clock();
const controls = new THREE.FlyControls(camera);
controls.dragToLook = true;
controls.movementSpeed = 8;
controls.rollSpeed = 0.5;

const hueDelta = 0.2;
var changingHue = false;

var lastMesh;
var prototypeGeometry = new THREE.BoxGeometry(1, 1, 1);
var prototypeMaterial = new THREE.MeshBasicMaterial({ color: 'red'});

var animationFrameID = 0;

function animate() {
  animationFrameID = requestAnimationFrame(animate);

  const dt = clock.getDelta();

  controls.update(dt);

  if (changingHue) {
    let hsl = prototypeMaterial.color.getHSL();
    hsl.h += hueDelta * dt;
    prototypeMaterial.color.setHSL(hsl.h, hsl.s, hsl.l);

    if (lastMesh) {
      lastMesh.material.color.setHSL(hsl.h, hsl.s, hsl.l);
    }
  }
  
  renderer.render(scene, camera);
}

function stopAnimation() {
  cancelAnimationFrame(animationFrameID);
  animationFrameID = 0;
}

function addCube() {
  var c = new THREE.Mesh(prototypeGeometry.clone(), prototypeMaterial.clone());
  c.position.add(camera.getWorldDirection().multiplyScalar(4).add(camera.position));
  scene.add(c);
  lastMesh = c;
}

window.addEventListener('resize', onWindowResize, false);

function onWindowResize() {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();
  renderer.setSize(window.innerWidth, window.innerHeight);
}

document.addEventListener('keypress', function (event) {
  switch (event.key) {
  case ' ':
    addCube();
    break;
  case 'c':
    changingHue = !changingHue;
    break;
  case 'x':
    const l = scene.children.length
    if (l > 0) {
      lastMesh = l < 2 ? undefined : scene.children[l - 2];
      scene.remove(scene.children[l - 1]);
    }
    break;
  }
});

addCube();
animate();

console.info('cube-paint.js: done');
